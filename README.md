This repository is a sqlite demo for MS3. 

The program takes in a CSV file, parses it and then writes full entries to a sqlite table
If there is an incomplete entry, it is instead written back out to another CSV file named bad-data