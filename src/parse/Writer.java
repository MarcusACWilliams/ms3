/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parse;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class Writer {
    
        public static void wr_to(String str) throws FileNotFoundException
    {
        PrintWriter pw = new PrintWriter(new File("bad.txt"));   
        pw.append(str);
        pw.append("\n");
        pw.close();
        
    }          
    
    
         public void write(String s) throws IOException {
         wr_to_log("log.txt", s);
         }
    
         public void wr_to_log(String f, String s) throws IOException {
         TimeZone tz = TimeZone.getTimeZone("EST"); // or PST, MID, etc ...
         Date now; 
         now = new Date();
         DateFormat df = new SimpleDateFormat ("yyyy.mm.dd hh:mm:ss ");
         df.setTimeZone(tz);
         String currentTime = df.format(now);
        
         FileWriter aWriter = new FileWriter(f, true);
         
         aWriter.write(currentTime + s + "\n");
         aWriter.flush();
         aWriter.close();
     }
}
