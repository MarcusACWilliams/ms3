/*
MS3 Interview Project
Marcus Williams

 */
package ms3;


import java.sql.*;
import java.io.*;
import java.io.BufferedReader;
import java.io.File;
import java.util.List;
import java.util.Scanner;
//My Packages
import parse.Parse_line;
import parse.Writer;

public class MS3 
{
//    private static final char DEFAULT_SEPARATOR = ',';
//    private static final char DEFAULT_QUOTE = '"';
    
 public static void main(String[] args) throws Exception
 {
    Connection c = null;
    Statement stmt = null;
    String importFile = "ms3Interview.csv";
    BufferedReader br = null;
    int i = 0;
    
    Parse_line parser = new Parse_line();
    Writer logger = new Writer();
    //testclass.test();
    try  // Open/Create database
    {
      Class.forName("org.sqlite.JDBC");
      c = DriverManager.getConnection("jdbc:sqlite:MS3.db");
      c.setAutoCommit(false);
      
    } 
    catch ( Exception e ) 
    {
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      System.exit(0);
    }
    System.out.println("Opened database successfully");
    
    
    
        DatabaseMetaData md = c.getMetaData();
        Scanner scanner = new Scanner(new File(importFile));
        Scanner user_input = new Scanner(System.in);
        List<String> line = parser.parseLine(scanner.nextLine()); // Discards Header Line
       
        BufferedWriter pw = new BufferedWriter(new OutputStreamWriter( new FileOutputStream("bad-data.csv"), "utf-8"));
            
        
        stmt = c.createStatement();
        String sql;
        ResultSet tables;
        Boolean flag = true, flag2 = true;
        int good_data = 0, bad_data = 0, total = 0, batch_count =0;
        
        tables = md.getTables(null, null, "%", null);
        
        while(tables.next())
        { 
            if (tables.getString(3).equals("GOOD"))
            {
                flag = false;  
            }
            if(tables.getString(3).equals("BAD"))
            {
                flag2 = false;
            }
        }    
            if(flag)// Create Table if One doesn't exist
            {
                sql = "CREATE TABLE GOOD " + "(A TEXT," + "B TEXT," + "C TEXT," +
                         "D TEXT," + "E TEXT," + "F TEXT," + "G TEXT," +
                         "H TEXT," + "I TEXT," + "J TEXT)" ;
                
                stmt.executeUpdate(sql);  
            }        
                
                System.out.println("Tables Created Succesfully");
                
                
                // Clears the GOOD data table before execution if you what to
                
                flag = true;
                while(flag)
                {
                System.out.println("Should I clear the table?(yes/no): ");
                String inputstring = user_input.nextLine();
                if(inputstring.equalsIgnoreCase("yes"))
                {sql = "DELETE FROM GOOD"; stmt.executeUpdate(sql); flag = false; System.out.println("Table Cleared!");}
                else if(inputstring.equalsIgnoreCase("no")){flag = false;}
                else{System.out.println("Please Enter yes or no...");}
                }
                
            stmt.close();
            
  
            
//*********** Start data parsing and insert *********************
            
            
        while (scanner.hasNext()) 
        {
            line = parser.parseLine(scanner.nextLine());
            stmt = c.createStatement();
           
          // Check if record is complete and send to MS3 table in data base  
          if(line.get(0).equals("false"))           /*!!!!!!!!!!!!Need to add a IF EXIST CHECK here*/
            {
             if(batch_count >= 500)  //Check if ove the batch limit and send querys if there are mroe than 500
             {
                batch_count = 0;
                stmt.executeBatch();
             }
                
            sql = "INSERT INTO GOOD (A,B,C,D,E,F,G,H,I,J) "+
                  "VALUES ('"+line.get(1)+"','" +line.get(2)+ "','" + line.get(3)+ "','" + line.get(4)+ "','" + line.get(5)+ "','" + line.get(6)+ "','" + line.get(7)+ "','" + line.get(8)+ "','" + line.get(9)+ "','" + line.get(10)+ "');"; 
            
            stmt.addBatch(sql);
            
            batch_count++;  
            good_data++;
            total++;
            
            }else // write record to bad data csv badData.csv
            {
              pw.write(line.get(1)+"," +line.get(2)+ "," + line.get(3)+ "," + line.get(4)+ "," + line.get(5)+ "," + line.get(6)+ "," + line.get(7)+ "," + line.get(8)+ "," + line.get(9)+ "," + line.get(10)); 
              pw.write("\n");
            
            bad_data++;
            total++;
            }
            i++;
        }
        
        if(batch_count > 0){stmt.executeBatch();}
        pw.close();
        System.out.println("data added");
        System.out.println("Good " + good_data+"\nBad " +bad_data+"\nTotal "+total);
        
        String to_log = "Total Records recived:" + total + " Files writen to database:" + good_data + " Files not written:" + bad_data+ "\n";
        
        logger.write(to_log); //Not printing New Line for some reason
        
        

        
        stmt.close();
        c.commit();
        c.close();
        scanner.close();
        

        

   }
 
 

         
 }
    
